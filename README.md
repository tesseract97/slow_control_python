# slow_control_py
Python scripts to automate a CouchDB slow control database and interface with CSV made of lab instrument datapoints.

**Input CSV File Specifications**

- each line of the CSV is one datapoint and each comma separated value is an attribute of the datapoint
- the first line of CSV must be the names of all the attributes being recorded (ex. timestamp, temperature, pressure)
- within the CSV, each comma separated value corresponds to the name specified in the first line
- attributes can be left blank, there just must be a space left for them in the csv (ex. 1,2,3,,4,5)
- one of the headers must be called "timestamp"
- upper limit of 4300 arguments (number of lines multiplied by number of values in the first line of the CSV)
- if the upper limit is respected, the script can handle any combination of number of lines and names of attributes
- no attribute name can be '_id'
- there must be consistency within each CSV file, but the next CSV file can have datapoints with completely different attributes as long as the "timestamp" specification is respected
- timestamp has been defined as Java regulated timestamp (ex. yyyy-mm-dd hh:mm:ss)

*Example CSV File*

![CSV_EXAMPLE](https://user-images.githubusercontent.com/47134315/86925837-585e7400-c0ff-11ea-827c-b58e111a6ee1.png)

*Example Resulting Document*

![Document](https://user-images.githubusercontent.com/47134315/86926411-05d18780-c100-11ea-9fa9-1f1bb069fda2.png)