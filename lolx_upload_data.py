import paramiko
import json
import csv
import os


class SlowControlConnection:

    def __init__(self):

        #ssh connection made with the host computer using credentials
        try:
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(hostname='132.206.126.208', port=2020, username='lolx', password='x3n0ntpc')

            #examples of how to call auxiliary functions
            #self.return_details(ssh)
            #self.write_value_to_database(ssh, "7")
            #self.see_all_databases(ssh)
            #self.format_and_make_string(self.csv_to_json())


            #embedded functions upload data from csv file to database
            self.write_to_database_with_data_string(ssh, self.format_and_make_string(self.csv_to_json()))
            #finally close the ssh connection
            ssh.close()
            #files are deleted to avoid conflict
            self.cleanup_directory()
        #ssh exceptions caught
        except paramiko.ssh_exception.SSHException:
            print("ssh exception")

    def return_details(self, ssh):
        #details of the CouchDB
        stdin, stdout, stderr = ssh.exec_command('curl http://127.0.0.1:5984/', get_pty=True)
        for line in stdout.readlines():
            print("CouchDB Details: ", line)

    def see_all_databases(self, ssh):
        #all databases that can be accessed by the CouchDB port are returned
        stdin, stdout, stderr = ssh.exec_command('curl http://admin:x3n0ntpc@127.0.0.1:5984/_all_dbs', get_pty=True)
        for line in stdout.readlines():
            print("Database List: ", line)


    def write_value_to_database(self, ssh, datapoint):
        #one value is written to the database "slow_control_test"
        data = "\'{\"_id\": \"\"}\'"
        data = data[:10] + datapoint + data[10:]
        command = 'curl -X POST http://admin:x3n0ntpc@127.0.0.1:5984/slow_control_test -d  -H "Content-Type: application/json"'
        command = command[:71] + data + command[71:]
        print(command)
        stdin, stdout, stderr = ssh.exec_command(command, get_pty=True)
        for line in stdout.readlines():
            print("Results: ", line)

    def csv_to_json(self):
        #turn csv file into json file
        #file names are specified
        csvFilePath = 'current_slow_control_data.csv'
        jsonFilePath = 'slow_control_update.json'
        data = []

        #test to make sure the CSV exists
        if os.path.exists('current_slow_control_data.csv'):
            #data is turned into a Python dictionary and the document id is set equal to the timestamp
            #then the dictionary is turned into a json file
            with open(csvFilePath, encoding='utf-8-sig') as csvFile:
                csvReader = csv.DictReader(csvFile)
                for rows in csvReader:
                    rows['_id'] = rows.get('timestamp')
                    data.append(rows)
            with open(jsonFilePath, 'w') as jsonFile:
                jsonFile.write(json.dumps(data, indent=4))
                return jsonFilePath
        else:
            return 0

    def format_and_make_string(self, file_name):
        #add escape characters to make command string
        #upper limit of 4300 arguments in the string (in the csv, rows*columns <= 4300)

        #no string is formatted if the file doesn't exist
        if(file_name == 0):
            return 0

        jsonFilePath = file_name
        f = open(jsonFilePath)
        data = json.load(f)

        data_string = {"docs": []}
        for rows in data:
            data_string["docs"].append(rows)

        #formats with escape characters so JSON format is observed
        string = str(data_string).replace("'", "\"").replace('"', '\\"')
        return '"' + string + '"'


    def write_to_database_with_data_string(self, ssh, data):

        #if data doesn't exist, message is sent to user
        if (data ==0):
            print("CSV file didn't exist")
            return 0

        # Write multiple documents to database using bulk docs API
        # the database being written to is "slow_control_test"
        # credentials are written into the code
        command = 'curl -X POST http://admin:x3n0ntpc@127.0.0.1:5984/slow_control_test/_bulk_docs -d  -H "Content-Type: application/json"'
        command = command[:82] + data + command[82:]
        print(command)
        stdin, stdout, stderr = ssh.exec_command(command, get_pty=True)
        for line in stdout.readlines():
            print("Results: ", line)

    def cleanup_directory(self):
        #JSON and CSV files are deleted so there's no conflict

        if os.path.exists('slow_control_update.json'):
            os.remove('slow_control_update.json')
            print("JSON deleted")
        else:
            print("JSON already deleted")

        if os.path.exists('current_slow_control_data.csv'):
            os.remove('current_slow_control_data.csv')
            print("CSV deleted")
        else:
            print("CSV already deleted")

if __name__ == '__main__':
    SlowControlConnection()